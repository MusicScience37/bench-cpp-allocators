#include <cstddef>
#include <memory_resource>
#include <unordered_map>
#include <vector>

#include <stat_bench/bench/invocation_context.h>
#include <stat_bench/benchmark_macros.h>
#include <stat_bench/util/do_not_optimize.h>

class Fixture : public stat_bench::FixtureBase {
public:
    Fixture() = default;

    template <typename Container>
    void fill(Container& container) {
        for (std::size_t i = 0; i < 10000; ++i) {
            container.try_emplace(i, i);
        }
    }
};

STAT_BENCH_CASE_F(Fixture, "unordered_map", "std_allocator") {
    STAT_BENCH_MEASURE() {
        std::unordered_map<std::size_t, std::size_t> container;
        fill(container);
    };
}

STAT_BENCH_CASE_F(Fixture, "unordered_map", "monotonic_buffer_resource") {
    STAT_BENCH_MEASURE() {
        std::pmr::monotonic_buffer_resource resource;
        auto allocator = std::pmr::polymorphic_allocator<
            std::pair<std::size_t, std::size_t>>(&resource);

        std::pmr::unordered_map<std::size_t, std::size_t> container(allocator);
        fill(container);
    };
}

STAT_BENCH_CASE_F(Fixture, "unordered_map", "unsynchronized_pool_resource") {
    STAT_BENCH_MEASURE() {
        std::pmr::unsynchronized_pool_resource resource;
        auto allocator = std::pmr::polymorphic_allocator<
            std::pair<std::size_t, std::size_t>>(&resource);

        std::pmr::unordered_map<std::size_t, std::size_t> container(allocator);
        fill(container);
    };
}

STAT_BENCH_CASE_F(
    Fixture, "unordered_map", "cached_monotonic_buffer_resource") {
    std::pmr::unsynchronized_pool_resource base_resource;
    STAT_BENCH_MEASURE() {
        std::pmr::monotonic_buffer_resource resource{&base_resource};
        auto allocator = std::pmr::polymorphic_allocator<
            std::pair<std::size_t, std::size_t>>(&resource);

        std::pmr::unordered_map<std::size_t, std::size_t> container(allocator);
        fill(container);
    };
}

STAT_BENCH_MAIN
